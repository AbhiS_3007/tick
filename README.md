#**Setting up TICK Stack for Sending Notifications  Corresponding to Various System Metric’s Spikes over Slack**


This file dictates the steps to setup TICK for sending notifications over slack corresponding to various system metric's spikes. The abbreviation 'TICK' stands for;

* **T**elegraf - The agent responsible for collecting and measuring various system metrics in real time
* **I**nfluxdb - The high-performance, time-series database that gets the stream of various metric’s values from telegraf and stores it
* **C**hronograf - The service responsible for displaying those metrics on a dashboard graphically
* **K**apacitor - The data processing engine that processes stream or batch data from influxdb, and is also used to send alerts

In order to establish the complete setup, please follow the steps below.

##**Setting up Telegraf for measuring system metrics**

**For this setup, telegraf was installed on a CentOS machine running inside a vagrant box on my local machine. This was done so that desirable stress testing could be performed to trigger notifications. Spin up a vagrant box having CentOS 7 and log into it using the command `vagrant up && vagrant ssh`. After this, in order to install telegraf on CentOS 7, follow this link [here](https://docs.influxdata.com/telegraf/v1.11/introduction/installation/).**
Once done, you will need to change the configuration file of telegraf, i.e., */etc/telegraf/telegraf.conf* file in order to align it with influxdb. Here are the changes that you’ll need to make;

1. Under the 'global_tags' heading inside the */home/telegrf/telegraf.conf* file, uncomment the first line. 
   ![image](https://drive.google.com/uc?export=view&id=16Pd86f70GoTLnkxTqN_6YKnieGeKaU8O)

2. Scroll further below in the same file to the 'output.influxdb' heading, under which you’ll need to add the url to the host machine on which influxdb will be running. Also you will need to uncomment the line that specifies the database name.
   ![image](https://drive.google.com/uc?export=view&id=1bDVZzJ3XmIa0X-V10g4o_bwlIg5Iq0VU)
   **In this setup, Influxdb, Chronograf and Kapacitor was ran using a docker-compose file that was executed on my local machine, so I had to specify my machine’s actual IP which can be easily obtained using the `ip addr` command along with the port to which influxdb was mapped from inside its docker container.**
3. Once done, start telegraf on CentOS machine using the command `sudo systemctl start telegraf` You might as well enable the service using the command `sudo systemctl enable telegraf`.

The telegraf setup is now complete.

---

##**Setting up Influxdb, Chronograf and Kapacitor using docker-compose;**

This setup was established using a docker-compose file and mounting various volumes to kapacitor container in order to put in custom *kapacitor.conf* file and various TICKscripts and topic handlers. Also, supervisor was also used in the kapacitor container to help the kapacitor service run in background and enable all the scripts and topic-handlers automatically without the need of manually enabeling them. All this is explained in the later part of this file. *The complete directory that was used along with the docker-compose file, the dockefile for kapacitor, the supervisor configuration file, the kapacitor configuration file to be run by supervisor, and the various mounted volumes that contain the kapacitor configuration file along with the TICKscripts are present in this repository. You can simply clone this repository on your local system*.

The docker-compose file that was used can be seen below.
![image](https://drive.google.com/uc?export=view&id=1_1_hYcyjPKl85_4JPGMGyk_bbki0Ga3g)

You need to have the latest version of docker and docker-compose on you local machine. **Also, there are some changes that you’ll need to make yourself in the */etc/kapacitor/kapacitor.conf* file before running docker-compose. Make those changes accordingly**.

![image](https://drive.google.com/uc?export=view&id=15593VVqjrd2sJLE-fROuk6nJ-nxL48pR)

1. Under the 'influxdb' heading, you need to mention the name that you used for influxdb service inside the docker-compose file in the **urls** option, since running docker-compose will create a new docker network and the containers are accessed with their respective service names in the docker network. For instance, I used 'influxdb' along with the port number to which kapacitor was mapped to in the docker-compose file.

2. Under the 'slack' heading, you need to replace the present webhook url with the webhook url of your slack channel in the **url** option.

For kapacitor, we will be using a custom dockerfile inside */kap-dockerfile/* directory. The file can be seen below.
![image](https://drive.google.com/uc?export=view&id=1D9EIWaSjIdGQghL_NuMgNjclShXrlJ1x)

Using this file, we are adding bash and supervisor service, copying configuration file for supervisor along with the kapacitor configuration file that tells supervisor to start the kapacitor service, a wait-for-it script that waits for the kapacitor service to start on port 9092 and a docker-entrypoint script. Supervisor config file along with kapacitor config for supervisor are present in the */etc/supervisor/* directory and can be seen below.

*/etc/supervisor/supervisord.conf*

![image](https://drive.google.com/uc?export=view&id=1D9syegCyLBdc71gGBv8bAiPvJyGQz3K7)

*/etc/supervisor/conf.d/kapacitor-sup.conf*

![image](https://drive.google.com/uc?export=view&id=1YqR714sPQGQ_R26iEQjqAWbW0l86qPar)

The docker-entrypoint file can be seen below.

![image](https://drive.google.com/uc?export=view&id=1xBz8-kE9x9PXi7qQ5yY71e7BmvRTOkw8)

The entrypoint script is responsible for running the supervisor service, which in turn starts the kapacitor service. Also, the wait-for-it script is also executed in the docker-entrypoint, its purpose being that it waits for the kapacitor service to start running so that TICKScripts and topic-handlers can be enabled through the docker-entrypoint. The last command that displays the kapacitor logs is responsible for keeping the kapacitor container alive, since supervisor runs kapacitor as a process in the background and there need to be some active process running in the container to keep it alive.




Kapacitor uses a domain specific language(DSL) called TICKscript which involves extraction, transformation and loading of data and involving, moreover the tracking of arbitrary changes and detection of events within data that is obtained from influxdb. The prominent task of kapacitor here would be to send alerts. TICKscript is used in `.tick` files to define pipelines for processing data. You can read more about this [here](https://docs.influxdata.com/kapacitor/v1.5/tick/introduction/).
In my setup, I used four TICKscripts for establishing various alerts, that can be found in this repository inside the */home/kapacitor/* directory. All these scripts are defined using kapacitor command in the docker-entrypoint file. One of the scripts that was used can be seen below.

![image](https://drive.google.com/uc?export=view&id=105jsSxH6cocVeMLYRLdeaEmZaacLwjfd)

The scripts that will be written by you will fairly follow the same format. Line by line explaination of the above script is as follow;

* Line 1 of the script specifies the database retention policy for the script. All the data coming into influxdb is retained/stored using retention policies that can be customized manually from the chronograf dashboard.
* Line 3 is used to specify the type of the system measurement that you need. You can choose from **cpu, disk, diskio, mem(memory), net(network), processes, swap, system**. There are further sub-metrics that you can select specifically, as can be seen in line 29 where `usage_system` and `usage_user` have been used. You can see detailed information about these [here](https://github.com/signalfx/integrations/tree/master/telegraf/docs).
* Lines 5 and 7 are used to specify options for the influxdb query that will extract the required data from the stream of all incoming data from telegraf that is already stored in influxdb. 
* Line 9 is where you can give a name to the alert, which is mapped as the ID for the alert.
* If you refer to lines 24-30, they show that kapacitor picks up data from influxdb with the help of a query. These lines are just a different format of a query itself. Data can be extracted as a **stream** or a **batch**. Here we are using data stream. Line 27 is the groupBy clause of the query using which you can group data according to the hosts,etc. And line 28 is the where clause of the query. 
* Line 29 is used to evaluate the cpu usage by system and user together in a single variable named value.

**NOTE : In the above image, you can see various strings are marked in green colour. These are *TICKscript Nodes*. These nodes are basically the means to extract, modify and direct all sorts of data of a system inside influxdb. For detailed information about nodes, please refer to this [link](https://docs.influxdata.com/kapacitor/v1.5/nodes/).**

* Lines 32-41 are responsible for sending the alert events. You can see that an *alert* node has been used in line 33. Documentation about alert node can be found [here](https://docs.influxdata.com/kapacitor/v1.5/nodes/alert_node). The *crit* node evaluates wether an alert is triggered or not depending upon evaluation of the condition. You can also choose a warn(warning) or an info(information) node. Out of these nodes, any of them can be specified any number of times. If none of them evaluates to true, then an alert event with *OK* level is passed. These nodes evaluate to a particular level of an alert that can be one of *OK*, *INFO*, *WARNING* or *CRITICAL*. Irrespective of the level of alert, the rest of nodes in lines 35-39 are attached to the alert anyway.

* The two most important nodes used here are the *stateChangeOnly* and *topic* node. Talking about stateChangeNode, when data is read from the data stream by an influxdb query, each data point in the stream is tested to see if it has hit the threshold or not. And corresponding to each data point that's above threshold an alert event is triggered. This causes the slack channel linked with the alerts to be flooded with notifications for every single data point that is above the threshold. To control this we use the *stateChangeOnly* node. It only forwards the alert event for the first data point above threshold and blocks all the rest alerts, until the level of alert event falls back to *OK*, corresponding to which a second notification will be sent telling that the metric has fallen below its threshold. It can be clearly deduced from the name of the node, that it only forwards an alert if the state/level has changed.

* **The `topic` node in line 41 is used to achieve higher functionality with alerts. Any alert event that is sent by the above script is published to the topic cpu. Further, *handlers* subscribe to these alerts. To implemet all this, we make a topic-handler that is responsible for each alert that is published to a topic.** For all the TICKScripts in this repository, a single topic 'cpu' was used, and its topic handler file is placed in */home/kapacitor/slack.yaml* directory. It can be seen below.
  
  ![image](https://drive.google.com/uc?export=view&id=1zDPQiHA-cTAuHQbFjsMC8Nn9Ik0-0CvM)

  A topic handler file is of *.yaml* format. It is defined in the docker-entrypoint file using the kapacitor command. The various components of this file are
  
  * id - The unique ID of the topic handler. 
  * topic - The topic related to this handler.
  * kind - Tells about the kind of alert event handler.
  * match - This holds a match expression that filters out the alert events that the handler processes. In our case, it only processes the alerts that have a level that is critical. More information [here](https://docs.influxdata.com/kapacitor/v1.5/working/alerts/#match-expressions)
  * options: Used to add extra information and parameters to the alert event.
  
  Detailed information about topic-handlers can be found [here](https://docs.influxdata.com/kapacitor/v1.5/working/using_alert_topics/).

After understanding all this, we can move ahead with the actual setup. Before this, **don't forget to clone this repository on your system. Also you must have telegraf configured and running on CentOS 7 inside a vagrant box**.

**1.** Move into the tick directory on your local system and run the command `docker-compose up -d`. Once done, open your browser and type the address to the chronograf service, that would be *localhost:9988*. You should see a screen like this.
![image](https://drive.google.com/uc?export=view&id=1D19LfRIJd8NtiQFLuB3BVCAa-I--geBs)
Press the get started button.

**2.** You should be seeing a screen like this. 
![image](https://drive.google.com/uc?export=view&id=1Zs63Jdoh-_F2NDKmIugWwRpZSxeaKIp-)
Change the connection url to *http://influxdb:8086* and a connection name of your choice. Once done click on the Add Connection button.

**3.** The screen shown below will appear. You can choose from a list of dashboard, but for this setup, we will use the system dashboard.
![image](https://drive.google.com/uc?export=view&id=1lIUEn0hidqG3dOaJDD_1XfnauEOHVtvB)
Select it and click on the Create 1 Dashboard button.

**4.** You'll be directed to the next screen as below.
![alert](https://drive.google.com/uc?export=view&id=1GtzPV2FOYDvBuJvQDcfEQbOQbhABSwrX)
Change the kapacitor url to *http://kapacitor:9092* and a name of your choice. Click on the continue button.

**5.** You'll be directed to a screen saying that the setup has been completed. Click on the View All Conncections button and you'll see a screen as below.
![image](https://drive.google.com/uc?export=view&id=1MpnxzHtNX7puzUmLDqmfXzZBIxwm3aM8)
If you click on the dashboard button,fourth from top on the left and select the dashboard by the name 'System', you'll be directed to a screen where you'll be able to see the system stats as seen in the image below.
![image](https://drive.google.com/uc?export=view&id=1Z9ClPHXLbK5QYAnX7vrASEoG3goc5Bau)


Once the above steps have been carried out successfully, all you have to do is simply go to the alerting option, fifth from top on the left, and you will be able to see all the scripts that have been defined. You can enable or disable them according to your convenience. The dashboard for the alerting option can be seen below.

![image](https://drive.google.com/uc?export=view&id=1e2Igx8JVwFLW-A40x5fgTMZwQQGmw76U)



In order to apply some stress on the CentOS machine to see if alerts are triggered, complete the above setup first, enable the scripts, go to the terminal where the CentOS machine is running and then you can use these two commands. You need to install *stress* and *stress-ng* on the CentOS machine first.

* `stress-ng -c 0 -l *percentage*` : for applying load on CPU.(percentage:0-100)
* `stress --vm-bytes $(awk '/MemFree/{printf "%d\n", $2 * 0.9;}' < /proc/meminfo)k --vm-keep -m 1` : for increasing memory usage. Here 0.9 standsfor 90% memory usage.
