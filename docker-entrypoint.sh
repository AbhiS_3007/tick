#!/bin/bash

# supervisor is responsible for running the kapacitor service. Thus, it is run at the beginning.
supervisord -c /etc/supervisor/supervisord.conf

# waiting for kapacitor service to run.
./wait-for-it.sh 127.0.0.1:9092 -t 0

# once kapacitor service starts, we define all the TICKScripts and topic-handlers.
kapacitor define cpu_usgae -type stream -tick ./home/kapacitor/cpu_usage.tick
kapacitor define system_load1 -type stream -tick ./home/kapacitor/system_load1.tick
kapacitor define-topic-handler ./home/kapacitor/slack.yaml

# since there are no active process in the container as supervisor is also running in the background, to keep the
# container from closing, we run the command below.
tail -f /kapacitor.log
